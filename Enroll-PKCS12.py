import base64
import datetime
import hashlib
import hmac
import json
import os
import subprocess
import sys
import urllib2

def setFile(filename, contents, mode="w+"):
  f = open(filename, mode)
  contents = f.write(contents)
  f.close()

def getResponse(endpoint, data, headers={}):
  request = urllib2.Request(endpoint)
  request.add_header("Authorization","Basic " + passphrase)
  request.add_header("Content-Type", "application/json")
  for header in headers.keys():
    request.add_header(header, headers[header])
  if data != None:
    request.add_data(data)
  response = urllib2.urlopen(request).read()
  return response
  
def issue12v3():
  endpoint = "http://<YOUR CMS ENDPOINT>/CMSAPI/CertEnroll/3/Pkcs12"
  now = datetime.datetime.utcnow()
  data = '{"Flags":0,"Pkcs12Password":"12341234","TemplateName":"UserServer","SubjectNameAttributes":[]}'
  data = '{"Timestamp" : "' + now.isoformat() + '", "request" : ' + data + '}'
  print len(data)
  h = hmac.new("00".decode("hex"),data,hashlib.sha1).hexdigest()
  print h
  headers = {}
  headers["X-CSS-CMS-AppKey"] = base64.b64encode("<YOUR_API_KEY>".decode("hex"))
  headers["X-CSS-CMS-AppSecret"] = base64.b64encode("<YOUR_API_Secret>".decode("hex"))
  headers["X-CSS-CMS-Signature"] = base64.b64encode(h.decode("hex"))
  response = json.loads(getResponse(endpoint, data, headers))["Pkcs12Blob"]
  setFile("/home/azureuser/response.pfx",response)
  print "Success"
  return True
  
passphrase = base64.b64encode("dom\\user:pass")
issue12v3()